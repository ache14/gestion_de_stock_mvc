package com.stock.mvc.entites;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class MvtStk implements Serializable{

	@Id
	@GeneratedValue
	private Long idMvtStk;

	public Long getIdMvtStk() {
		return idMvtStk;
	}

	public void setIdMvtStk(Long idMvtStk) {
		this.idMvtStk = idMvtStk;
	}
	
}
